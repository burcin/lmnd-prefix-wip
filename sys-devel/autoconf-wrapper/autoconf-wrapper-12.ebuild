# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /data/temp/gentoo//vcs-public-cvsroot/gentoo-x86/sys-devel/autoconf-wrapper/autoconf-wrapper-12.ebuild,v 1.9 2012/04/26 11:58:46 aballier Exp $

inherit multilib

DESCRIPTION="wrapper for autoconf to manage multiple autoconf versions"
HOMEPAGE="http://www.gentoo.org/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ia64 m68k ~mips ppc ppc64 s390 sh sparc x86 ~ppc-aix ~amd64-fbsd ~sparc-fbsd ~x86-fbsd ~x64-freebsd ~x86-freebsd ~hppa-hpux ~ia64-hpux ~x86-interix ~amd64-linux ~ia64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~m68k-mint ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"
IUSE=""

S=${WORKDIR}

src_install() {
	exeinto /usr/$(get_libdir)/misc
	newexe "${FILESDIR}"/ac-wrapper-${PV}.sh ac-wrapper.sh || die

	dodir /usr/bin
	local x=
	for x in auto{conf,header,m4te,reconf,scan,update} ifnames ; do
		dosym ../$(get_libdir)/misc/ac-wrapper.sh /usr/bin/${x} || die
	done
}

pkg_postinst() {
	# added for prefix
	# the wrapper wants to find each version as a suffix to the name
	# it is called by
	for x in /usr/bin/auto{conf,header,reconf,scan,update}?*; do
		if [ -f $x ]; then
			target=$(basename ${x})
			verline=$(${x} --version|head -n1)
			version=${verline//[^0-9.]}
			name=${target//[0-9.-]}
			if [ ! -e "${EPREFIX}/bin/$name-$version" ]; then
				einfo "Linking $name-$version to ${x}"
				ln -sf "${x}" "${EPREFIX}/bin/$name-$version"
			fi
		fi
	done
}

