# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /data/temp/gentoo//vcs-public-cvsroot/gentoo-x86/profiles/arch/amd64-fbsd/todo/package.use.mask,v 1.250 2012/05/26 17:23:34 aballier Exp $

# Mask useflags that require deps with missing keywords.
# Anyone is allowed and _very welcome_ to add masks there.
# We will pick them up and keyword as time permits and prefer having a sane
# deptree with missing features than a broken one.
# Do *NOT* add a ChangeLog entry when modifying this file: it is intended as
# temporary and cvs log is more than enough for getting its history.
# 
# Syntax is: # mising dep(s)
#            mask
# Keep it simple, we do not really care about the date or who masked it, we want
# to be easily able to determine what we need to keyword.

# net-libs/axtls
net-misc/curl curl_ssl_axtls

# sys-libs/ldb
net-fs/samba ldb

# dev-lang/php:5.3
dev-libs/ossp-uuid php

# dev-lang/php
media-libs/libvpx doc

# sys-auth/pam_krb5
sys-auth/pambase pam_krb5

# dev-lang/mono
net-dns/libidn mono
media-libs/libcaca mono
dev-java/antlr mono

# dev-lang/mono
# >=dev-dotnet/gtk-sharp-2
net-dns/avahi mono

# net-misc/netkit-rsh
x11-apps/xsm rsh

# dev-libs/libnl:1.1
net-libs/libpcap libnl

# net-libs/libtirpc
sys-apps/xinetd rpc

# media-gfx/ufraw
media-gfx/imagemagick raw

# >=media-libs/portaudio-19_pre
media-libs/openal portaudio

# dev-libs/cyrus-sasl
dev-vcs/subversion sasl
net-nds/openldap sasl cxx

# app-crypt/heimdal
net-nds/openldap smbkrb5passwd

# app-office/lyx
dev-tex/latex-beamer lyx

# net-libs/libproxy
net-libs/neon libproxy
net-libs/glib-networking libproxy

# dev-libs/pakchois
net-libs/neon pkcs11

# media-libs/lasi
media-gfx/graphviz lasi

# app-text/texlive[extra]
app-doc/doxygen latex

# dev-tex/latex2html
dev-python/pyopenssl doc
dev-tex/chktex doc

# dev-util/source-highlight
dev-util/gtk-doc highlight

# sys-cluster/openmpi
dev-libs/boost mpi

# x11-misc/colord
x11-libs/gtk+ colord

# sys-auth/polkit
sys-auth/consolekit policykit
gnome-base/gconf policykit

# media-sound/pulseaudio
media-libs/libsdl pulseaudio
media-libs/openal pulseaudio
media-video/ffmpeg pulseaudio
media-libs/libcanberra pulseaudio
media-libs/phonon pulseaudio

# >=sys-fs/udisks-1.90:2
gnome-base/gvfs udisks

# >=app-pda/libimobiledevice-1.1.0
gnome-base/gvfs ios

# >=sys-fs/fuse-2.8.0
gnome-base/gvfs fuse

# >=gnome-base/libgdu-3.0.2
gnome-base/gvfs gdu

# >=x11-libs/libva-0.32
media-video/ffmpeg vaapi
virtual/ffmpeg vaapi

# >=media-libs/xvid-1.1.0
media-video/ffmpeg xvid

# media-plugins/frei0r-plugins
media-video/ffmpeg frei0r

# media-libs/nas
media-libs/libsdl nas
x11-libs/qt-gui nas

# net-misc/openntpd
net-misc/ntp openntpd

# virtual/mysql
sci-mathematics/glpk mysql
dev-libs/apr-util mysql
x11-libs/qt-sql mysql
dev-libs/redland mysql

# dev-db/freetds
dev-libs/apr-util freetds
x11-libs/qt-sql freetds

# >=gnome-base/libgnomeui-2
# >=gnome-base/libbonoboui-2
dev-util/glade gnome

# app-emulation/wine
media-libs/mesa d3d

# sys-fs/udev
media-libs/mesa gbm
x11-drivers/xf86-video-intel glamor

# does not build
x11-libs/libdrm libkms
media-libs/mesa video_cards_vmware
media-libs/mesa video_cards_r600
media-libs/mesa video_cards_radeon

# x11-drivers/xf86-video-sis
x11-base/xorg-drivers video_cards_sis

# x11-drivers/xf86-video-tdfx
x11-base/xorg-drivers video_cards_tdfx

# x11-drivers/xf86-video-vmware
# x11-libs/libdrm[libkms,video_cards_vmware]
x11-base/xorg-drivers video_cards_vmware

# >=media-plugins/gst-plugins-ffmpeg-0.10
media-plugins/gst-plugins-meta ffmpeg

# >=media-plugins/gst-plugins-pulse-0.10
media-plugins/gst-plugins-meta pulseaudio

# media-plugins/gst-plugins-taglib
media-plugins/gst-plugins-meta taglib

# >=dev-cpp/clucene-0.9.21[-debug]
app-misc/strigi clucene
dev-libs/soprano clucene

# >=media-libs/phonon-vlc-0.3.2
media-libs/phonon vlc

# >=sys-auth/polkit-qt-0.103.0
# >=kde-misc/polkit-kde-kcmodules-0.98_pre20101127
# >=sys-auth/polkit-kde-agent-0.99
kde-base/kdelibs policykit

# <=sci-geosciences/gpsd-2.95-r1
app-misc/geoclue gps

# app-misc/geoclue[networkmanager]
app-misc/geoclue skyhook

# >=sys-devel/binutils-2.22
sys-devel/llvm gold
