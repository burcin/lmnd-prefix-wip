# search library paths in the prefix before those on the system
# LIBRARY_PATH variable is not useful here since those are searched after
# the system library paths known to gcc. The contents of this variable are
# appended to the arguments passed to gcc or ld. Paths given on the command
# line have priority over any system paths.
export COMPILERWRAPPER_LDFLAGS=-L${LMNDBASE}/local/lib
# compilerwrapper uses this variable to determine the absolute library path
# for the prefix.
export SAGE_LOCAL=${LMNDBASE}/local
